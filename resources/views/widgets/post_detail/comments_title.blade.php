<p class="text-muted"><i class="fa fa-comment" aria-hidden="true"></i>
    <small>
        <!-- Ajout de commantaire -->
        @if($post->getCommentCount() > 0)
            @if($post->getCommentCount() > 1){{ $post->getCommentCount().' comments' }}@else{{ $post->getCommentCount().' commentaire' }}@endif
        @else
         Sans commentaires! Écrire un commentaire
        @endif
    </small>
</p>
<hr>
<!-- Tout les commentaire -->
@if($post->getCommentCount() > 2 && (empty($comment_count) || $comment_count < 3))
    <a class="btn btn-link btn-block btn-xs" href="{{ url('/post/'.$post->id) }}"><i class="fa fa-bars" aria-hidden="true"></i> Afficher tous les commentaires</a>
@endif