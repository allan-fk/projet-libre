<div class="menu">
    <ul class="list-group">
        <li class="list-group-item">
            <a href="{{ url('/') }}" class="menu-home">
                <i class="fa fa-home"></i>
                Page d'acceuil
            </a>
        </li>
        <li class="list-group-item">
        </li>
        <li class="list-group-item">
            <a href="{{ url('/direct-messages') }}" class="menu-dm">
                <i class="fa fa-commenting"></i>
                Message privé
            </a>
        </li>
    </ul>
</div>