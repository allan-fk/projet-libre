<?php

use Illuminate\Http\Request;

if(version_compare(PHP_VERSION, '7.2.0', '>=')) {
    error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);
}
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
